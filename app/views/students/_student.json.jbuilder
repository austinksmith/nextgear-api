json.extract! student, :id, :name, :enrolled, :age, :created_at, :updated_at
json.url student_url(student, format: :json)